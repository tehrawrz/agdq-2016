package com.shuneault.agdq2016;

import android.os.AsyncTask;
import android.text.Html;
import android.util.Log;

import com.shuneault.agdq2016.objects.GameSchedule;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by sebast on 01/01/16.
 */
public class WebCodeSource extends AsyncTask<Void, Integer, ArrayList<GameSchedule>> {

    public interface OnThreadProgress {
        void onProgress(Integer progress);
        void onFinish(ArrayList<GameSchedule> gameSchedule);
    }

    private static final String URL_SCHEDULE = "https://gamesdonequick.com/schedule";
//    private static final String URL_SCHEDULE = "http://192.168.1.103:3000";
    private static final String RE_SCHEDULE = "<tr>\\n<td.*?>(.*?)?</td>\\n<td.*?>(.*?)?</td>\\n<td.*?>(.*?)?</td>(\\n<td.*?>(.*?)?</td>\\n<td.*?>(.*?)?</td>\\n<td.*?>(.*?)?</td>\\n<td.*?>(.*?)?</td>\\n)?</tr>";

    private final OnThreadProgress mListener;

    public WebCodeSource(OnThreadProgress listener) {
        mListener = listener;
    }

    @Override
    protected ArrayList<GameSchedule> doInBackground(Void... params) {
        ArrayList<GameSchedule> theSchedule = new ArrayList<>();

        try {
            URL url = new URL(URL_SCHEDULE);
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setUseCaches(false);

            StringBuilder strCode = new StringBuilder();
            int b;
            while ( (b = httpURLConnection.getInputStream().read() ) != -1) {
                strCode.append((char)b);
            }

            try {
                Pattern p = Pattern.compile(RE_SCHEDULE, Pattern.CASE_INSENSITIVE);
                Matcher m = p.matcher(strCode);
                while (m.find()) {
                    String sDate = Html.fromHtml(m.group(1)).toString();
                    String sGame = Html.fromHtml(m.group(2)).toString();
                    String sRunners = Html.fromHtml(m.group(3)).toString();
                    String sRunTime = Html.fromHtml(m.group(5)).toString();
                    String sCategory = Html.fromHtml(m.group(6)).toString();
                    String sSetupTime = Html.fromHtml(m.group(7)).toString();
                    String sDescription = Html.fromHtml(m.group(8)).toString();
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(new SimpleDateFormat("y-M-d H:m:s Z").parse(sDate.replace("Z", " -0000").replace("T", " ")));
                    theSchedule.add(new GameSchedule(sGame, cal, sRunners, sRunTime, sCategory, sSetupTime, sDescription));
                }
            } catch (ParseException e) {
                e.printStackTrace();
                Log.i("LOGCAT", "ERROR: " + e.getMessage());
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
            Log.i("LOGCAT", "ERROR: " + e.getMessage());
        } catch (IOException e) {
            e.printStackTrace();
            Log.i("LOGCAT", "ERROR: " + e.getMessage());
        }

        Collections.sort(theSchedule, new Comparator<GameSchedule>() {
            @Override
            public int compare(GameSchedule lhs, GameSchedule rhs) {
                return lhs.getDate().compareTo(rhs.getDate());
            }
        });
        return theSchedule;
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        mListener.onProgress(values[0]);
    }

    @Override
    protected void onPostExecute(ArrayList<GameSchedule> gameSchedules) {
//        super.onPostExecute(gameSchedules);
        Log.i("LOGCAT", "onPostExecute: " + gameSchedules.size());
        mListener.onFinish(gameSchedules);
    }
}
