package com.shuneault.agdq2016.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.shuneault.agdq2016.AppSingleton;
import com.shuneault.agdq2016.R;
import com.shuneault.agdq2016.intentservices.RefreshBroadcastReceiver;
import com.shuneault.agdq2016.objects.GameSchedule;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class GameScheduleFragment extends Fragment {

    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    private static final String ARG_DATE = "ARG_DATE";
    // TODO: Customize parameters
    private int mColumnCount = 2;
    private String mGameDate;
    private OnListFragmentInteractionListener mListener;
    private ArrayList<GameSchedule> mGameList;
    private AppSingleton mySingleton;
    private RecyclerView mRecyclerView;

    private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, final Intent intent) {
            mySingleton = AppSingleton.getInstance(context);
            GameScheduleFragment.this.receivedBroadcast(intent);
        }
    };

    private void receivedBroadcast(Intent intent) {
        refreshGameList();
        mRecyclerView.getAdapter().notifyDataSetChanged();
        Log.i("LOGCAT", "receivedBroadcast");
    }

    private void refreshGameList() {
        mGameList.clear();
        DateFormat df = SimpleDateFormat.getDateInstance();
        for (GameSchedule gameSchedule : mySingleton.getGameList()) {
            if (mGameDate.equals(df.format(gameSchedule.getDate().getTime()))) {
                mGameList.add(gameSchedule);
            }
        }
    }

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public GameScheduleFragment() {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static GameScheduleFragment newInstance(String gameDate) {
        GameScheduleFragment fragment = new GameScheduleFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, 1);
        args.putString(ARG_DATE, gameDate);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
            mGameDate = getArguments().getString(ARG_DATE);
        }

        // Singleton
        mySingleton = AppSingleton.getInstance(getContext());

        // Generate game list for the selected day
        mGameList = new ArrayList<>();
        refreshGameList();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_gameschedule_list, container, false);

        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            mRecyclerView = (RecyclerView) view;

            if (mColumnCount <= 1) {
                mRecyclerView.setLayoutManager(new LinearLayoutManager(context));
            } else {
                mRecyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
            }
            mRecyclerView.setAdapter(new MyGameScheduleRecyclerViewAdapter(mGameList, mySingleton.getReminders(), mListener));
        }

        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onResume() {
        super.onResume();
        IntentFilter filter = new IntentFilter();
        filter.setPriority(1);
        filter.addAction(RefreshBroadcastReceiver.BROADCAST_REFRESH_GAMELIST);
        getActivity().registerReceiver(mBroadcastReceiver, filter);
        refreshGameList();
        mRecyclerView.getAdapter().notifyDataSetChanged();
    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(mBroadcastReceiver);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        void onListFragmentInteraction(GameSchedule item);
        void onListReminderCheckChanged(GameSchedule item, boolean isChecked);
    }
}
