package com.shuneault.agdq2016.fragments;

import com.shuneault.agdq2016.AppSingleton;
import com.shuneault.agdq2016.R;

import android.content.res.ColorStateList;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.shuneault.agdq2016.fragments.GameScheduleFragment.OnListFragmentInteractionListener;
import com.shuneault.agdq2016.fragments.dummy.DummyContent.DummyItem;
import com.shuneault.agdq2016.objects.GameSchedule;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display a {@link DummyItem} and makes a call to the
 * specified {@link OnListFragmentInteractionListener}.
 * TODO: Replace the implementation with code for your data type.
 */
public class MyGameScheduleRecyclerViewAdapter extends RecyclerView.Adapter<MyGameScheduleRecyclerViewAdapter.ViewHolder> {

    private final ArrayList<GameSchedule> mValues;
    private final HashMap<String, GameSchedule> mReminders;
    private final OnListFragmentInteractionListener mListener;

    public MyGameScheduleRecyclerViewAdapter(ArrayList<GameSchedule> items, HashMap<String, GameSchedule> reminders, OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
        mReminders = reminders;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_gameschedule, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        DateFormat df = new SimpleDateFormat("hh:mm a");
        holder.mItem = mValues.get(position);
        holder.mIdView.setText(df.format(holder.mItem.getDate().getTime()));
        holder.mContentView.setText(holder.mItem.getName());
        holder.mDescription.setText(holder.mItem.getDescription());
        holder.mChkReminder.setChecked(mReminders.containsKey(holder.mItem.getName()));
        holder.mChkReminder.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mReminders.put(holder.mItem.getName(), holder.mItem);
                } else {
                    mReminders.remove(holder.mItem.getName());
                }
                mListener.onListReminderCheckChanged(holder.mItem, isChecked);
            }
        });
        // Hide the reminder checkbox if time is passed
        if (System.currentTimeMillis() > holder.mItem.getDate().getTimeInMillis()) {
            holder.mChkReminder.setVisibility(View.GONE);
        } else {
            holder.mChkReminder.setVisibility(View.VISIBLE);
        }

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mIdView;
        public final TextView mContentView;
        public final TextView mDescription;
        public final CheckBox mChkReminder;
        public GameSchedule mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mIdView = (TextView) view.findViewById(R.id.lblGameTime);
            mContentView = (TextView) view.findViewById(R.id.lblGameName);
            mChkReminder = (CheckBox) view.findViewById(R.id.chkGameSchedule);
            mDescription = (TextView) view.findViewById(R.id.lblDescription);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mContentView.getText() + "'";
        }
    }
}
