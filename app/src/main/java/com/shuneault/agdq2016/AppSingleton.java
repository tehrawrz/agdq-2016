package com.shuneault.agdq2016;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.shuneault.agdq2016.intentservices.RefreshBroadcastReceiver;
import com.shuneault.agdq2016.objects.GameSchedule;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by sebast on 31/12/15.
 */
public class AppSingleton {
    private static AppSingleton ourInstance = new AppSingleton();
    private Context mContext = null;
    private HashMap<String, GameSchedule> mReminders = new HashMap<>();
    private ArrayList<GameSchedule> mGameList = new ArrayList<>();

    private static final String SHARED_PREF_AGDQ = "SHARED_PREF_AGDQ";
    private static final String PREF_REMINDERS = "PREF_REMINDERS";
    private static final String PREF_SCHEDULE = "PREF_SCHEDULE";
    private static final String FILE_REMINDERS = "reminders.data";
    private static final String FILE_SCHEDULE = "schedule.html";

    public static AppSingleton getInstance(Context context) {
        if (ourInstance.mContext != null) return ourInstance;

        ourInstance.mContext = context;
        ourInstance.loadGameList();
        ourInstance.loadReminders();

        // Automatic Refresh
        AlarmManager mgr = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, RefreshBroadcastReceiver.class);
        PendingIntent sender = PendingIntent.getBroadcast(context, 1, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        mgr.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), 1000 * 60 * 15, sender); // Update every 15 minutes
        return ourInstance;
    }

    private AppSingleton() {
    }


    public boolean addReminder(GameSchedule gameSchedule) {
        mReminders.put(gameSchedule.getName(), gameSchedule);
        return saveReminders();
    }

    public boolean removeReminder(String gameName) {
        mReminders.remove(gameName);
        return saveReminders();
    }

    public void clearReminders() {
        mReminders.clear();
        saveReminders();
    }

    public ArrayList<GameSchedule> getGameList() {
        return mGameList;
    }

    public void clearGames() {
        mGameList.clear();
        saveGameList();
    }

    public void addGame(GameSchedule gameSchedule) {
        mGameList.add(gameSchedule);
        saveGameList();
    }

    public void addAllGames(ArrayList<GameSchedule> gameSchedules) {
        mGameList.addAll(gameSchedules);
        saveGameList();
    }


    public HashMap<String, GameSchedule> getReminders() {
        return mReminders;
    }

    public boolean saveGameList() {
        if (mContext == null) return false;
        Gson gson = new Gson();
        mContext.getSharedPreferences(SHARED_PREF_AGDQ, Context.MODE_PRIVATE)
                .edit()
                .putString(PREF_SCHEDULE, gson.toJson(mGameList))
                .commit();
        return true;
    }

    public boolean loadGameList() {
        if (mContext == null) return false;
        Gson gson = new Gson();
        mGameList = gson.fromJson(mContext.getSharedPreferences(SHARED_PREF_AGDQ, Context.MODE_PRIVATE)
                .getString(PREF_SCHEDULE, ""), new TypeToken<ArrayList<GameSchedule>>(){}.getType());
        if (mGameList == null) mGameList = new ArrayList<>();
        return true;
    }

    public boolean saveReminders() {
        if (mContext == null) return false;
        Gson gson = new Gson();
        mContext.getSharedPreferences(SHARED_PREF_AGDQ, Context.MODE_PRIVATE)
                .edit()
                .putString(PREF_REMINDERS, gson.toJson(mReminders))
                .commit();
        return true;
    }

    public boolean loadReminders() {
        if (mContext == null) return false;
        Gson gson = new Gson();
        mReminders = gson.fromJson(mContext.getSharedPreferences(SHARED_PREF_AGDQ, Context.MODE_PRIVATE)
                .getString(PREF_REMINDERS, ""), new TypeToken<HashMap<String, GameSchedule>>(){}.getType());
        if (mReminders == null) mReminders = new HashMap<>();
        // Remove old reminders
        for (GameSchedule game : mReminders.values()) {
            if (System.currentTimeMillis() > game.getDate().getTimeInMillis()) {
                mReminders.remove(game.getName());
            }
        }
        return true;
    }


}
