package com.shuneault.agdq2016.alarms;

import com.shuneault.agdq2016.AppSingleton;
import com.shuneault.agdq2016.R;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;

/**
 * Created by sebast on 29/07/15.
 */
public class ScheduledService extends IntentService {

    public ScheduledService() {
        super("SGDQ Alarm");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        PendingIntent twitchPending = PendingIntent.getActivity(this, 0, new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.twitch.tv/gamesdonequick")), PendingIntent.FLAG_ONE_SHOT);

        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext());
        builder.setContentText("AGDQ 2016");
        builder.setSmallIcon(R.mipmap.ic_launcher);
        builder.setContentTitle(intent.getStringExtra("Game") + " is currently playing");
        builder.setDefaults(Notification.DEFAULT_VIBRATE | Notification.DEFAULT_SOUND | Notification.FLAG_ONLY_ALERT_ONCE);
        builder.setTicker(intent.getStringExtra("Game"));
        builder.addAction(R.drawable.ic_twitch, "Watch on Twitch", twitchPending);
        builder.setAutoCancel(true);
        Notification notification = builder.build();
        notificationManager.notify(intent.getIntExtra("GameId", 0), notification);

        // Delete the alarm
        Intent i = new Intent(this, ScheduledService.class);
        intent.putExtra("Game", intent.getStringArrayExtra("Game"));
        intent.putExtra("GameId", intent.getIntExtra("GameId", 0));
        PendingIntent sender = PendingIntent.getService(this, intent.getIntExtra("GameId", 0), i, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager mgr = (AlarmManager) this.getSystemService(this.ALARM_SERVICE);
        mgr.cancel(sender);
    }
}
